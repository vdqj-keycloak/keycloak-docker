<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="./images/keycloak.jpeg" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Proyecto Keycloak In Docker</h3>

  <p align="center">
    Descripción del proyecto!
    <br />
    <a href="https://github.com/othneildrew/Best-README-Template"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://github.com/othneildrew/Best-README-Template">View Demo</a>
    ·
    <a href="https://github.com/othneildrew/Best-README-Template/issues">Report Bug</a>
    ·
    <a href="https://github.com/othneildrew/Best-README-Template/issues">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>


<!-- ABOUT THE PROJECT -->
## Acerca del proyecto

Documentación para levantar keycloak con docker y agregar un proveedor de indetificación

# Foobar

Foobar is a Python library for dealing with word pluralization.

## Installation

En el proyecto esta un manifiesto para levantar keycloak en un contenedor
```bash
$ sudo docker compose up -d
```

## Copiar jar:proveedor de indentificacion en el contenedor

Copiar el jar del proveedor al contenedor de keycloak:
```bash
$ sudo docker cp user-storage-properties-example.jar 478124fc51fa:/opt/keycloak/providers
```

Copiar el jar de la libreria fastjson-1.2.57.jar para mapear al contenedor de keycloak:
```bash
$ sudo docker cp fastjson-1.2.57.jar 478124fc51fa:/opt/keycloak/providers
```

Ingresar al contenedor de keycloak corriendo:
```bash
$ sudo docker exec -it 478124fc51fa bash
```

Ejecutar el jar en el contenedor:
```bash
$ cd /opt/keycloak/bin
$ ./kc.sh build
```

Resetear el contenedor para ver el provider creado
```bash
$ sudo docker restart 478124fc51fa
```

---
name: "Victor Quino"
about: La Paz / Aduana Nacional
labels: prueba

---

## Contributing

Gracias

## License

[MIT](https://choosealicense.com/licenses/mit/)
